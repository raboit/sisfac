<?php

class ProveedorController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Proveedor'),
		));
	}
        
        public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Proveedor'),
		));
	}
        
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('Proveedor');  // 'User' is classname of model to be updated
            $es->update();
        }
        
        public function actionGetTodos(){
            $data= Proveedor::model()->findAll(array('order'=>'id DESC'));
            $data=CHtml::listData($data,'id','nombre');
             foreach($data as $value=>$name)
                {
                    echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                 
                }

        }

        public function actionAgregar(){
		
            $model = new Proveedor;
            
		if (isset($_POST['Proveedor'])) {
			$model->setAttributes($_POST['Proveedor']);			
			$model->save();
                        if (Yii::app()->request->isAjaxRequest){
                            echo CJSON::encode(array('status'=>'success','div'=>"Proveedor creado exitosamente"));
                            exit;               
                        }
                        
						
		}		
		if (Yii::app()->request->isAjaxRequest)
                    {
                        echo CJSON::encode(array('status'=>'failure', 'div'=>$this->renderPartial('_agregar', array('model' => $model,),true)));
                        exit;               
                    }
	}
        
	public function actionCreate() {
		$model = new Proveedor;

		$this->performAjaxValidation($model, 'proveedor-form');

		if (isset($_POST['Proveedor'])) {
			$model->setAttributes($_POST['Proveedor']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new Proveedor;

		$this->performAjaxValidation($model, 'proveedor-form');

		if (isset($_POST['Proveedor'])) {
			$model->setAttributes($_POST['Proveedor']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Proveedor');

		$this->performAjaxValidation($model, 'proveedor-form');

		if (isset($_POST['Proveedor'])) {
			$model->setAttributes($_POST['Proveedor']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Proveedor');

		$this->performAjaxValidation($model, 'proveedor-form');

		if (isset($_POST['Proveedor'])) {
			$model->setAttributes($_POST['Proveedor']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Proveedor')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Proveedor');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Proveedor('search');
		$model->unsetAttributes();

		if (isset($_GET['Proveedor'])){
			$model->setAttributes($_GET['Proveedor']);
                }

                $session['Proveedor_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new Proveedor('search');
		$model->unsetAttributes();

		if (isset($_GET['Proveedor'])){
			$model->setAttributes($_GET['Proveedor']);
                }

                $session['Proveedor_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Proveedor_model_search']))
               {
                $model = $session['Proveedor_model_search'];
                $model = Proveedor::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Proveedor::model()->findAll();
             
             $this->toExcel($model, array('id', 'rut', 'nombre'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Proveedor_model_search']))
               {
                $model = $session['Proveedor_model_search'];
                $model = Proveedor::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Proveedor::model()->findAll();
             
             $this->toExcel($model, array('id', 'rut', 'nombre'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}