<?php

class DefaultController extends Controller
{
    
        public function filters() {
                return array('rights');
        }
	public function actionIndex()
	{
		$this->render('index');
	}
        public function actionSolicitud()
	{
            $item_solicitud = new ItemSolicitud;
            $item_solicitud = $item_solicitud->misItems(Yii::app()->getRequest()->getParam('id'));
		$this->renderPartial('/solicitud/_items', array(
                    'item_solicitud' => $item_solicitud,
            ));
	}
}