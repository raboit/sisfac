<?php

class TipoIngresoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'TipoIngreso'),
		));
	}
        
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('TipoIngreso');  // 'User' is classname of model to be updated
            $es->update();
        }

	public function actionCreate() {
		$model = new TipoIngreso;

		$this->performAjaxValidation($model, 'tipo-ingreso-form');

		if (isset($_POST['TipoIngreso'])) {
			$model->setAttributes($_POST['TipoIngreso']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'TipoIngreso');

		$this->performAjaxValidation($model, 'tipo-ingreso-form');

		if (isset($_POST['TipoIngreso'])) {
			$model->setAttributes($_POST['TipoIngreso']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'TipoIngreso')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('TipoIngreso');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new TipoIngreso('search');
		$model->unsetAttributes();

		if (isset($_GET['TipoIngreso'])){
			$model->setAttributes($_GET['TipoIngreso']);
                }

                $session['TipoIngreso_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['TipoIngreso_model_search']))
               {
                $model = $session['TipoIngreso_model_search'];
                $model = TipoIngreso::model()->findAll($model->search()->criteria);
               }
               else
                 $model = TipoIngreso::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['TipoIngreso_model_search']))
               {
                $model = $session['TipoIngreso_model_search'];
                $model = TipoIngreso::model()->findAll($model->search()->criteria);
               }
               else
                 $model = TipoIngreso::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}