<?php

class AdminController extends GxController
{
        public $layout='//layouts/column1';
        
	public function init()
	{
		$this->defaultAction = 'index';
	}        
        
        public function filters() {
                return array('rights');
        }
	public function actionIndex()
	{
		$this->render('index');
	}
}