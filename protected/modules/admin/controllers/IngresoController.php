<?php

class IngresoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Ingreso'),
		));
	}
        
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('Ingreso');  // 'User' is classname of model to be updated
            $es->update();
        }

	public function actionCreate() {
		$model = new Ingreso;

		$this->performAjaxValidation($model, 'ingreso-form');

		if (isset($_POST['Ingreso'])) {
			$model->setAttributes($_POST['Ingreso']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Ingreso');

		$this->performAjaxValidation($model, 'ingreso-form');

		if (isset($_POST['Ingreso'])) {
			$model->setAttributes($_POST['Ingreso']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Ingreso')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Ingreso');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Ingreso('search');
		$model->unsetAttributes();

		if (isset($_GET['Ingreso'])){
			$model->setAttributes($_GET['Ingreso']);
                }

                $session['Ingreso_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Ingreso_model_search']))
               {
                $model = $session['Ingreso_model_search'];
                $model = Ingreso::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Ingreso::model()->findAll();
             
             $this->toExcel($model, array('id', 'proyecto', 'tipoIngreso', 'financiamiento', 'fecha', 'monto', 'observaciones'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Ingreso_model_search']))
               {
                $model = $session['Ingreso_model_search'];
                $model = Ingreso::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Ingreso::model()->findAll();
             
             $this->toExcel($model, array('id', 'proyecto', 'tipoIngreso', 'financiamiento', 'fecha', 'monto', 'observaciones'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}