<?php

class ProyectoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Proyecto'),
		));
	}
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('Proyecto');  // 'User' is classname of model to be updated
            $es->update();
        }

	public function actionCreate() {
		$model = new Proyecto;

		$this->performAjaxValidation($model, 'proyecto-form');

		if (isset($_POST['Proyecto'])) {
			$model->setAttributes($_POST['Proyecto']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Proyecto');

		$this->performAjaxValidation($model, 'proyecto-form');

		if (isset($_POST['Proyecto'])) {
			$model->setAttributes($_POST['Proyecto']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Proyecto')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Proyecto');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Proyecto('search');
		$model->unsetAttributes();

		if (isset($_GET['Proyecto'])){
			$model->setAttributes($_GET['Proyecto']);
                }

                $session['Proyecto_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Proyecto_model_search']))
               {
                $model = $session['Proyecto_model_search'];
                $model = Proyecto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Proyecto::model()->findAll();
             
             $this->toExcel($model, array('id', 'rrhh', 'centroCosto', 'nombre', 'descripcion', 'archivo', 'contrato', 'comuna', 'region'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Proyecto_model_search']))
               {
                $model = $session['Proyecto_model_search'];
                $model = Proyecto::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Proyecto::model()->findAll();
             
             $this->toExcel($model, array('id', 'rrhh', 'centroCosto', 'nombre', 'descripcion', 'archivo', 'contrato', 'comuna', 'region'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}