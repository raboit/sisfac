<?php

class EmisionController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Emision'),
		));
	}
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('Emision');  // 'User' is classname of model to be updated
            $es->update();
        }

	public function actionCreate() {
		$model = new Emision;

		$this->performAjaxValidation($model, 'emision-form');

		if (isset($_POST['Emision'])) {
			$model->setAttributes($_POST['Emision']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Emision');

		$this->performAjaxValidation($model, 'emision-form');

		if (isset($_POST['Emision'])) {
			$model->setAttributes($_POST['Emision']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Emision')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Emision');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Emision('search');
		$model->unsetAttributes();

		if (isset($_GET['Emision'])){
			$model->setAttributes($_GET['Emision']);
                }

                $session['Emision_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Emision_model_search']))
               {
                $model = $session['Emision_model_search'];
                $model = Emision::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Emision::model()->findAll();
             
             $this->toExcel($model, array('id', 'user', 'proveedor', 'proyecto', 'solicitud', 'ciudad', 'fecha', 'concepto', 'total_debe', 'total_haber', 'cuenta', 'tipo_solicitud', 'numero_cheque', 'observaciones', 'estado'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Emision_model_search']))
               {
                $model = $session['Emision_model_search'];
                $model = Emision::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Emision::model()->findAll();
             
             $this->toExcel($model, array('id', 'user', 'proveedor', 'proyecto', 'solicitud', 'ciudad', 'fecha', 'concepto', 'total_debe', 'total_haber', 'cuenta', 'tipo_solicitud', 'numero_cheque', 'observaciones', 'estado'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}