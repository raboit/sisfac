<?php

class RendicionController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Rendicion'),
		));
	}
        
        public function actionEditable()
        {
            Yii::import('ext.x-editable.EditableSaver'); //or you can add import 'ext.editable.*' to config
            $es = new EditableSaver('Rendicion');  // 'User' is classname of model to be updated
            $es->update();
        }

	public function actionCreate() {
		$model = new Rendicion;

		$this->performAjaxValidation($model, 'rendicion-form');

		if (isset($_POST['Rendicion'])) {
			$model->setAttributes($_POST['Rendicion']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Rendicion');

		$this->performAjaxValidation($model, 'rendicion-form');

		if (isset($_POST['Rendicion'])) {
			$model->setAttributes($_POST['Rendicion']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Rendicion')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Rendicion');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Rendicion('search');
		$model->unsetAttributes();

		if (isset($_GET['Rendicion'])){
			$model->setAttributes($_GET['Rendicion']);
                }

                $session['Rendicion_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Rendicion_model_search']))
               {
                $model = $session['Rendicion_model_search'];
                $model = Rendicion::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Rendicion::model()->findAll();
             
             $this->toExcel($model, array('id', 'user', 'emision', 'asignacion', 'fecha', 'comuna', 'region', 'tipo_pago', 'total_entrada', 'total_salida', 'total_saldo', 'observacion', 'estado'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Rendicion_model_search']))
               {
                $model = $session['Rendicion_model_search'];
                $model = Rendicion::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Rendicion::model()->findAll();
             
             $this->toExcel($model, array('id', 'user', 'emision', 'asignacion', 'fecha', 'comuna', 'region', 'tipo_pago', 'total_entrada', 'total_salida', 'total_saldo', 'observacion', 'estado'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}