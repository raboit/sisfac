<?php

$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered',
	'dataProvider' => $model->all(),
	'template' => "{items}{pager}",
	'columns' => array(
		'id',
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'fecha',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('financiamiento/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'format' => 'dd/mm/yyyy',
                                'viewformat'  => 'dd/mm/yyyy',
                            'type'=>'date',
                            
			)
		),
            array(
			'class' => 'editable.EditableColumn',
			'name' => 'monto',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('financiamiento/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),
            array(
			'class' => 'editable.EditableColumn',
			'name' => 'observaciones',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('financiamiento/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'textarea',
                            
			)
		),
            array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{delete}',			
				
                ),

		
            
            ),
));

?>