<?php

$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered',
	'dataProvider' => $model->all(),
	'template' => "{items}{pager}",
	'columns' => array(
		'id',
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'nombre',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('proveedor/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),
                array(
			'class' => 'editable.EditableColumn',
			'name' => 'rut',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('proveedor/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),
            array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{delete}',			
				
                ),
		
            
            ),
));

?>