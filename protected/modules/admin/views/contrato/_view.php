<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('rrhh_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->rrhh)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('proyecto_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->proyecto)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo_contrato_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->tipoContrato)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_inicio')); ?>:
	<?php echo GxHtml::encode($data->fecha_inicio); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_termino')); ?>:
	<?php echo GxHtml::encode($data->fecha_termino); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('remuneracion')); ?>:
	<?php echo GxHtml::encode($data->remuneracion); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('estado')); ?>:
	<?php echo GxHtml::encode($data->estado); ?>
	<br />
	*/ ?>

</div>