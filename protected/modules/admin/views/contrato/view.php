<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'rrhh',
			'type' => 'raw',
			'value' => $model->rrhh !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->rrhh)), array('rrhh/view', 'id' => GxActiveRecord::extractPkValue($model->rrhh, true))) : null,
			),
array(
			'name' => 'proyecto',
			'type' => 'raw',
			'value' => $model->proyecto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->proyecto)), array('proyecto/view', 'id' => GxActiveRecord::extractPkValue($model->proyecto, true))) : null,
			),
array(
			'name' => 'tipoContrato',
			'type' => 'raw',
			'value' => $model->tipoContrato !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->tipoContrato)), array('tipoContrato/view', 'id' => GxActiveRecord::extractPkValue($model->tipoContrato, true))) : null,
			),
'fecha_inicio',
'fecha_termino',
'remuneracion',
'estado',
	),
)); ?>

