<?php
// example code for the view "_relational" that returns the HTML content
echo CHtml::tag('h4',array(),'ITEMS DE SOLICITUD : ');
$this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered',
	'dataProvider' => $item_solicitud,
	'template' => "{items}",
	'columns' => array(
            'id',  
            'descripcion',
            'valor_unitario',
            'cantidad',
            'total',
        ),
));

?>