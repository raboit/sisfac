<?php

$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered',
	'dataProvider' => $model->all(),
	'template' => "{items}",
	'columns' => array(
                'id',                
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'fecha',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('solicitud/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'type'=>'date',
                                'format' => 'dd/mm/yyyy',
                                'viewformat'  => 'dd/mm/yyyy',
                            
			)
		),
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'total',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('solicitud/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'type'=>'text',
                            
			)
		),
                array(
			'class' => 'editable.EditableColumn',
			'name' => 'estado',
			'editable' => array(
				'url' => $this->createUrl('solicitud/editable'),
                                'type'=>'select',
                                'source'=>array('ANULADA'=>'ANULADA','EMITIDA'=>'EMITIDA','PENDIENTE'=>'PENDIENTE','FINALIZADA'=>'FINALIZADA'),
                            
			),
		),
            array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{delete}',			
				
                ),
            ),
));

?>