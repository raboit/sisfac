<?php

$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered',
	'dataProvider' => $model->all(),
	'template' => "{items}",
	'columns' => array(
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'nombre',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('proyecto/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'descripcion',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('proyecto/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'textarea',
                            
			)
		),
            'archivo',
            'contrato',
            array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{delete}',			
				
                ),
            
            ),
));
?>