<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'item-rendicion-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'proveedor_id'); ?>
		<?php echo $form->dropDownList($model, 'proveedor_id', GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'proveedor_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_contable_id'); ?>
		<?php echo $form->dropDownList($model, 'cuenta_contable_id', GxHtml::listDataEx(CuentaContable::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'cuenta_contable_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta_especifica_id'); ?>
		<?php echo $form->dropDownList($model, 'cuenta_especifica_id', GxHtml::listDataEx(CuentaEspecifica::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'cuenta_especifica_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'rendicion_id'); ?>
		<?php echo $form->dropDownList($model, 'rendicion_id', GxHtml::listDataEx(Rendicion::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'rendicion_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proyecto_id'); ?>
		<?php echo $form->dropDownList($model, 'proyecto_id', GxHtml::listDataEx(Proyecto::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'proyecto_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_documento'); ?>
		<?php echo $form->textField($model, 'tipo_documento', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'tipo_documento'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_documento'); ?>
		<?php echo $form->textField($model, 'numero_documento', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'numero_documento'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'mes'); ?>
		<?php echo $form->textField($model, 'mes', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'mes'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'agno'); ?>
		<?php echo $form->textField($model, 'agno', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'agno'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'monto'); ?>
		<?php echo $form->textField($model, 'monto'); ?>
		<?php echo $form->error($model,'monto'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->