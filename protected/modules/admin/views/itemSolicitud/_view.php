<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('solicitud_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->solicitud)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('descripcion')); ?>:
	<?php echo GxHtml::encode($data->descripcion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('valor_unitario')); ?>:
	<?php echo GxHtml::encode($data->valor_unitario); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cantidad')); ?>:
	<?php echo GxHtml::encode($data->cantidad); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total')); ?>:
	<?php echo GxHtml::encode($data->total); ?>
	<br />

</div>