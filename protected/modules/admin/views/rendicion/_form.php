<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'rendicion-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(User::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'user_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'emision_id'); ?>
		<?php echo $form->dropDownList($model, 'emision_id', GxHtml::listDataEx(Emision::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'emision_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'asignacion'); ?>
		<?php echo $form->textField($model, 'asignacion', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'asignacion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comuna'); ?>
		<?php echo $form->textField($model, 'comuna', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'comuna'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'region'); ?>
		<?php echo $form->textField($model, 'region', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'region'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_pago'); ?>
		<?php echo $form->textField($model, 'tipo_pago', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'tipo_pago'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_entrada'); ?>
		<?php echo $form->textField($model, 'total_entrada'); ?>
		<?php echo $form->error($model,'total_entrada'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_salida'); ?>
		<?php echo $form->textField($model, 'total_salida'); ?>
		<?php echo $form->error($model,'total_salida'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_saldo'); ?>
		<?php echo $form->textField($model, 'total_saldo'); ?>
		<?php echo $form->error($model,'total_saldo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'observacion'); ?>
		<?php echo $form->textField($model, 'observacion'); ?>
		<?php echo $form->error($model,'observacion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->textField($model, 'estado', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'estado'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('itemRendicions')); ?></label>
		<?php echo $form->checkBoxList($model, 'itemRendicions', GxHtml::encodeEx(GxHtml::listDataEx(ItemRendicion::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->