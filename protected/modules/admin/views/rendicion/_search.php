<?php $this->widget('ext.EChosen.EChosen' ); ?><div class="wide form">

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'search-rendicion-form',
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'emision_id'); ?>
		<?php echo $form->dropDownList($model, 'emision_id', GxHtml::listDataEx(Emision::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'asignacion'); ?>
		<?php echo $form->textField($model, 'asignacion', array('maxlength' => 100)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
                	<div class="row">
                <?php echo $form->label($model, 'fecha_inicio'); ?>
                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_inicio',
			'value' => $model->fecha_inicio,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
	<div class="row">
                <?php echo $form->label($model, 'fecha_termino'); ?>
    
                <?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha_termino',
			'value' => $model->fecha_termino,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'comuna'); ?>
		<?php echo $form->textField($model, 'comuna', array('maxlength' => 100)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'region'); ?>
		<?php echo $form->textField($model, 'region', array('maxlength' => 100)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'tipo_pago'); ?>
		<?php echo $form->textField($model, 'tipo_pago', array('maxlength' => 100)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'total_entrada'); ?>
		<?php echo $form->textField($model, 'total_entrada'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'total_salida'); ?>
		<?php echo $form->textField($model, 'total_salida'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'total_saldo'); ?>
		<?php echo $form->textField($model, 'total_saldo'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'observacion'); ?>
		<?php echo $form->textField($model, 'observacion'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'estado'); ?>
		<?php echo $form->textField($model, 'estado', array('maxlength' => 45)); ?>
	</div>
                
	<div class="row buttons">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>Yii::t('app', 'Search'), 'icon'=>'search'));?>
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'label'=>Yii::t('app', 'Reset'), 'icon'=>'icon-remove-sign', 'htmlOptions'=>array('class'=>'btnreset btn-small')));?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
     $(".btnreset").click(function(){
             $(":input","#search-rendicion-form").each(function() {
             var type = this.type;
             var tag = this.tagName.toLowerCase(); // normalize case
             if (type == "text" || type == "password" || tag == "textarea") this.value = "";
             else if (type == "checkbox" || type == "radio") this.checked = false;
             else if (tag == "select") this.selectedIndex = "";
       });
     });
</script>

