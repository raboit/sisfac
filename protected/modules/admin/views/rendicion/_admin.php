<?php
$model = new Rendicion('search');
$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered',
	'dataProvider' => $model->all(),
	'template' => "{items}",
	'columns' => array(
                'id',
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'fecha',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('rendicion/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'format' => 'dd/mm/yyyy',
                                'viewformat'  => 'dd/mm/yyyy',
                                'type'=>'date',
                            
			)
		),
                array(
			'class' => 'editable.EditableColumn',
			'name' => 'asignacion',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('rendicion/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'type'=>'select',
                                'source'=>array('Multiple'=>'Multiple','Directa'=>'Directa')
                            
			)
		),
		'comuna',
                array(
			'class' => 'editable.EditableColumn',
			'name' => 'estado',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('rendicion/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'type'=>'select',
                                'source'=>array('ANULADA'=>'ANULADA','ACTIVA'=>'ACTIVA','FINALIZADA'=>'FINALIZADA')
                            
			)
		),
            array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{delete}',			
				
                ),
		
            
            ),
));
?>
