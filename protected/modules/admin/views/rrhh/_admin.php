<?php

$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered',
	'dataProvider' => $model->all(),
	'template' => "{items}",
	'columns' => array(
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'nombres',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('emision/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),
                array(
			'class' => 'editable.EditableColumn',
			'name' => 'apellidos',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('emision/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),
                array(
			'class' => 'editable.EditableColumn',
			'name' => 'rut',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('emision/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),		
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'sexo',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('rendicion/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'type'=>'select',
                                'source'=>array('Masculino'=>'Masculino','Femenino'=>'Femenino')
                            
			)
		),
            array(
			'class' => 'editable.EditableColumn',
			'name' => 'fecha_nacimiento',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('emision/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'format' => 'dd/mm/yyyy',
                                'viewformat'  => 'dd/mm/yyyy',
                            'type'=>'date',
                            
			)
		),
            array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{delete}',			
				
                ),
		
            
            ),
));

?>