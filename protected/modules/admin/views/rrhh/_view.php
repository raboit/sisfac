<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('rut')); ?>:
	<?php echo GxHtml::encode($data->rut); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('nombres')); ?>:
	<?php echo GxHtml::encode($data->nombres); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('apellidos')); ?>:
	<?php echo GxHtml::encode($data->apellidos); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sexo')); ?>:
	<?php echo GxHtml::encode($data->sexo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha_nacimiento')); ?>:
	<?php echo GxHtml::encode($data->fecha_nacimiento); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('direccion')); ?>:
	<?php echo GxHtml::encode($data->direccion); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('telefono')); ?>:
	<?php echo GxHtml::encode($data->telefono); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('celular')); ?>:
	<?php echo GxHtml::encode($data->celular); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('email')); ?>:
	<?php echo GxHtml::encode($data->email); ?>
	<br />
	*/ ?>

</div>