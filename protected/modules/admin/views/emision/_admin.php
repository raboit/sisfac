<?php

$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered',
	'dataProvider' => $model->all(),
	'template' => "{items}",
	'columns' => array(
            'id',
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'ciudad',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('emision/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'fecha',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('emision/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'format' => 'dd/mm/yyyy',
                                'viewformat'  => 'dd/mm/yyyy',
                            'type'=>'date',
                            
			)
		),
                array(
			'class' => 'editable.EditableColumn',
			'name' => 'tipo_solicitud',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('emision/editable'),
                                'type'=>'select',
                                'source'=>array('Cheque'=>'Cheque','Transferencia'=>'Transferencia'),
                            
			),
		),
            array(
			'class' => 'editable.EditableColumn',
			'name' => 'numero_cheque',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('emision/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                            'type'=>'text',
                            
			)
		),

               array(
			'class' => 'editable.EditableColumn',
			'name' => 'estado',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('emision/editable'),
				'placement' => 'right',
				'inputclass' => 'span3',
                                'type'=>'select',
                                'source'=>array('ANULADA'=>'ANULADA','RENDIDA'=>'RENDIDA','FINALIZADA'=>'FINALIZADA')
                            
			)
		),
            array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{delete}',			
				
                ),
		
            
            ),
));

?>