<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'emision-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(User::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'user_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proveedor_id'); ?>
		<?php echo $form->dropDownList($model, 'proveedor_id', GxHtml::listDataEx(Proveedor::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'proveedor_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'proyecto_id'); ?>
		<?php echo $form->dropDownList($model, 'proyecto_id', GxHtml::listDataEx(Proyecto::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'proyecto_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'solicitud_id'); ?>
		<?php echo $form->dropDownList($model, 'solicitud_id', GxHtml::listDataEx(Solicitud::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'solicitud_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ciudad'); ?>
		<?php echo $form->textField($model, 'ciudad', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'ciudad'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'concepto'); ?>
		<?php echo $form->textField($model, 'concepto', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'concepto'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_debe'); ?>
		<?php echo $form->textField($model, 'total_debe'); ?>
		<?php echo $form->error($model,'total_debe'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_haber'); ?>
		<?php echo $form->textField($model, 'total_haber'); ?>
		<?php echo $form->error($model,'total_haber'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cuenta'); ?>
		<?php echo $form->textField($model, 'cuenta', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'cuenta'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'tipo_solicitud'); ?>
		<?php echo $form->textField($model, 'tipo_solicitud', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'tipo_solicitud'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_cheque'); ?>
		<?php echo $form->textField($model, 'numero_cheque', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'numero_cheque'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textField($model, 'observaciones'); ?>
		<?php echo $form->error($model,'observaciones'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->textField($model, 'estado', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'estado'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('rendicions')); ?></label>
		<?php echo $form->checkBoxList($model, 'rendicions', GxHtml::encodeEx(GxHtml::listDataEx(Rendicion::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->