<?php

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<h1>Modulo de Administraci&oacute;n</h1> <br>
<p class="well">
    Bienvenido a la secci&oacute;n de  <a href="#" rel="tooltip" title="Administrar todos los recursos">Administraci&oacute;n</a>,
    la cual le permite administrar todos los recursos de Sisproy.<br>
    Favor s&iacute;rvase a leer el <a href="#" rel="tooltip" title="Parte 4.3.1 del Manual">manual de usuario</a> para utilizar este m&oacute;dulo. Recuerde que
    cualquier cambio realizado puede afectar el funcionamiento y consistencia del Sistema.
</p>

<?php $this->widget('bootstrap.widgets.TbTabs', array(
    'type'=>'tabs',
    'placement'=>'left', // 'above', 'right', 'below' or 'left'
    'tabs'=>array(
        array('label'=>'Proyecto', 'content'=> $this->renderPartial('/proyecto/_admin', array('model' => new Proyecto,),$this), 'active'=>true),
        array('label'=>'Solicitud', 'content'=>$this->renderPartial('/solicitud/_admin', array('model' => new Solicitud,),$this)),
        array('label'=>'Emision', 'content'=>$this->renderPartial('/emision/_admin', array('model' => new Emision,),$this)),
        array('label'=>'Rendicion', 'content'=>$this->renderPartial('/rendicion/_admin', array('model' => new Rendicion),$this)),
        array('label'=>'Presupuesto', 'content'=>$this->renderPartial('/presupuesto/_admin', array('model' => new Presupuesto,),$this)),
        array('label'=>'RRHH', 'content'=>$this->renderPartial('/rrhh/_admin', array('model' => new Rrhh,),$this)),
        array('label'=>'Centro Costo', 'content'=>$this->renderPartial('/centrocosto/_admin', array('model' => new CentroCosto,),$this)),
        array('label'=>'Cuenta Contable', 'content'=>$this->renderPartial('/cuentacontable/_admin', array('model' => new CuentaContable,),$this)),
        array('label'=>'Cuenta Especifica', 'content'=>$this->renderPartial('/cuentaespecifica/_admin', array('model' => new CuentaEspecifica,),$this)),
        array('label'=>'Proveedor', 'content'=>$this->renderPartial('/proveedor/_admin', array('model' => new Proveedor,),$this)),
        array('label'=>'Financiamiento', 'content'=>$this->renderPartial('/financiamiento/_admin', array('model' => new Financiamiento,),$this)),
        array('label'=>'Ingreso', 'content'=>$this->renderPartial('/ingreso/_admin', array('model' => new Ingreso,),$this)),
        array('label'=>'Tipo Ingreso', 'content'=>$this->renderPartial('/tipoingreso/_admin', array('model' => new TipoIngreso,),$this)),
        array('label'=>'Usuario', 'content'=>$this->renderPartial('/usuario/_admin', array('model' => new User,),$this)),
    ),
)); ?>


