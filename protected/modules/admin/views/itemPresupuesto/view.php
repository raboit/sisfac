<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'presupuesto',
			'type' => 'raw',
			'value' => $model->presupuesto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->presupuesto)), array('presupuesto/view', 'id' => GxActiveRecord::extractPkValue($model->presupuesto, true))) : null,
			),
array(
			'name' => 'cuentaContable',
			'type' => 'raw',
			'value' => $model->cuentaContable !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cuentaContable)), array('cuentaContable/view', 'id' => GxActiveRecord::extractPkValue($model->cuentaContable, true))) : null,
			),
'fecha',
'mes',
'agno',
array(
                'name' => 'Monto',
                'value' => Yii::app()->format->formatNumber($model->monto),
),
array(
			'name' => 'proyecto',
			'type' => 'raw',
			'value' => $model->proyecto !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->proyecto)), array('proyecto/view', 'id' => GxActiveRecord::extractPkValue($model->proyecto, true))) : null,
			),
	),
)); ?>

