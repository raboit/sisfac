<?php

$this->breadcrumbs = array(
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('cliente-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'cliente-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'rut',
		'nombre',
		'direccion',
                'telefono',		
		'ciudad',
		'giro',
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view}{update}{delete}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Cliente',
					'url'=>'Yii::app()->createUrl("cliente/ver", array("id"=>$data->id))',
				),
                                'update' => array(
					'label'=>'Actualizar Cliente',
					'url'=>'Yii::app()->createUrl("cliente/actualizar", array("id"=>$data->id))',
				),
                                'delete' => array(
					'label'=>'Eliminar Cliente',
					'url'=>'Yii::app()->createUrl("cliente/eliminar", array("id"=>$data->id))',
				),
				
			),
				
                ),
	),
)); ?>