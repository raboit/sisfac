<?php

$this->breadcrumbs = array(
	$model->label(2) => array('verTodos'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('eliminar', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
        array('label'=>Yii::t('app', 'Back'), 'url'=>Yii::app()->request->urlReferrer),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'rut',
'nombre',
'direccion',
'telefono',
'ciudad',
'giro',
	),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_factura,
    'enableSorting'=>false,
    'template'=> '{summary}{pager}{items}{pager}',
    'columns' => array(
		'fecha',
                'guia_despacho',
                'condiciones_venta',
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 60px'),
			'template'=>'{view} {update} {delete}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Factura',
					'url'=>'Yii::app()->createUrl("factura/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Factura',
					'url'=>'Yii::app()->createUrl("factura/actualizar", array("id"=>$data->id))',
				),
                                'delete' => array(
					'label'=>'Eliminar Factura',
					'url'=>'Yii::app()->createUrl("factura/eliminar", array("id"=>$data->id))',
				),
			),
				
                ),
    ),
));?>