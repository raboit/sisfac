<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'factura-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'cliente_id'); ?>
		<?php echo $form->dropDownList($model, 'cliente_id', GxHtml::listDataEx(Cliente::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'cliente_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
                        'language' => Yii::app()->language,    
			'options' => array(
				'showButtonPanel' => true,
                                'changeMonth' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'guia_despacho'); ?>
		<?php echo $form->textField($model, 'guia_despacho', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'guia_despacho'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'condiciones_venta'); ?>
		<?php echo $form->textField($model, 'condiciones_venta', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'condiciones_venta'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'valor_neto'); ?>
		<?php echo $form->textField($model, 'valor_neto'); ?>
		<?php echo $form->error($model,'valor_neto'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'iva'); ?>
		<?php echo $form->textField($model, 'iva'); ?>
		<?php echo $form->error($model,'iva'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total'); ?>
		<?php echo $form->textField($model, 'total'); ?>
		<?php echo $form->error($model,'total'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numero_factura'); ?>
		<?php echo $form->textField($model, 'numero_factura'); ?>
		<?php echo $form->error($model,'numero_factura'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('detalles')); ?></label>
		<?php echo $form->checkBoxList($model, 'detalles', GxHtml::encodeEx(GxHtml::listDataEx(Detalle::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->