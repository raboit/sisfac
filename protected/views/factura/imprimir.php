<style type="text/css">
 @media print {
    .oculto {display:none}
  }
</style>

<a class="oculto" style="position: absolute;left: 240px;" href="<?php echo Yii::app()->request->urlReferrer; ?>">Volver</a>
<input type="button" style="position: absolute;left: 300px;" class="oculto" name="imprimir" value="Imprimir" onclick="window.print();">

<div id="contenido" style=" font-size: 11px;">
<div id="fecha_dia" style="position: absolute;left: 126px; top: 115px"><?php echo Yii::app()->dateFormatter->format('dd',$model->fecha); ?></div>
<div id="fecha_mes" style="position: absolute;left: 240px; top: 115px"><?php echo Yii::app()->dateFormatter->format('MMMM',$model->fecha); ?></div>
<div id="fecha_aÃ±o" style="position: absolute;left: 350px; top: 115px"><?php echo Yii::app()->dateFormatter->format('yy',$model->fecha); ?></div>
<div id="nombre_cliente" style="position: absolute;left:142px; top: 131px;font-size: 8px; "><?php  echo $model->cliente->nombre; ?></div>
<div id="direccion_cliente" style="position: absolute;left:142px; top: 146px ;font-size: 8px;"><?php echo $model->cliente->direccion; ?></div>
<div id="giro_cliente" style="position: absolute;left:140px; top: 162px;font-size: 8px;"><?php echo $model->cliente->giro; ?></div>
<div id="guia_despacho" style="position: absolute;left:240px; top: 178px"><?php  echo $model->guia_despacho; ?></div>
<div id="rut_cliente" style="position: absolute;left:632px; top: 131px"><?php  echo $model->cliente->rut; ?></div>
<div id="ciudad" style="position: absolute;left:632px; top: 146px"><?php echo $model->cliente->ciudad; ?></div>
<div id="telefono_cliente" style="position: absolute;left:632px; top: 162px"><?php echo $model->cliente->telefono; ?></div>
<div id="condiciones_venta" style="position: absolute;left:632px; top: 178px; font-size: 8px;"><?php  echo $model->condiciones_venta; ?></div>


<?php 
foreach ($model->detalles as $key => $detalle) :
?>
<?php 
$posicion= (($key+1)*16)+206; ?>
<div id="cantidad_<?php echo $key?>" style="position: absolute;left:140px; top:<?php echo $posicion; ?>px"><?php echo $detalle->cantidad; ?></div><br>
<div id="detalle_<?php echo $key?>" style="position: absolute;left:200px; top: <?php echo $posicion; ?>px"><?php echo $detalle->servicio->nombre; ?></div><br>
<div id="preciounitario_<?php echo $key?>" style="position: absolute;left:590px; top: <?php echo $posicion; ?>px"><?php echo Yii::app()->format->formatNumber($detalle->precio_unit); ?></div><br>
<div id="preciototal_<?php echo $key?>" style="position: absolute;left:680px; top: <?php echo $posicion; ?>px"><?php echo Yii::app()->format->formatNumber($detalle->total); ?></div><br>
<?php
endforeach;
?>


<div id="valor_neto" style="position: absolute;left:650px; top: 338px"><?php echo Yii::app()->format->formatNumber($model->valor_neto); ?></div>
<div id="iva" style="position: absolute;left:650px; top: 354px"><?php echo Yii::app()->format->formatNumber($model->iva); ?></div>
<div id="total" style="position: absolute;left:650px; top: 370px"><?php echo Yii::app()->format->formatNumber($model->total); ?></div>
</div>