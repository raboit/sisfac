
<?php

$this->breadcrumbs = array(
	$model->label(2) => array('verTodos'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('eliminar', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('verTodos')),
        array('label'=>Yii::t('app', 'Imprimir') . ' ' . $model->label(), 'url'=>array('imprimir', 'id' => $model->id)),
        array('label'=>Yii::t('app', 'Crear Detalle'), 'url'=>array('detalle/crear', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Back'), 'url'=>Yii::app()->request->urlReferrer),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(

            array(
                                    'name' => 'cliente',
                                    'type' => 'raw',
                                    'value' => $model->cliente !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cliente)), array('cliente/ver', 'id' => GxActiveRecord::extractPkValue($model->cliente, true))) : null,
                                    ),
            'fecha',
            'guia_despacho',
            'condiciones_venta',
            array(
                'name'=>'valor_neto',
                'type'=>'raw',
                'value'=>Yii::app()->format->formatNumber($model->valor_neto)
                ),
            array(
                'name'=>'iva',
                'type'=>'raw',
                'value'=>Yii::app()->format->formatNumber($model->iva)
                ),
            array(
                'name'=>'total',
                'type'=>'raw',
                'value'=>Yii::app()->format->formatNumber($model->total)
                ),
                    ),
            )); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_detalle,
    'enableSorting'=>false,
    'template'=> '{summary}{pager}{items}{pager}',
    'columns' => array(
		'cantidad',
                'servicio',
                
                array(
                'name'=>'precio_unit',
                'type'=>'raw',
                'value'=>'Yii::app()->format->formatNumber($data->precio_unit)'
                ),
        
                array(
                'name'=>'total',
                'type'=>'raw',
                'value'=>'Yii::app()->format->formatNumber($data->total)'
                ),
                
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 60px'),
			'template'=>'{view} {update} {delete}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Detalle',
					'url'=>'Yii::app()->createUrl("detalle/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Detalle',
					'url'=>'Yii::app()->createUrl("detalle/actualizar", array("id"=>$data->id))',
				),
                                'delete' => array(
					'label'=>'Eliminar Detalle',
					'url'=>'Yii::app()->createUrl("detalle/eliminar", array("id"=>$data->id))',
				),
			),
				
                ),
    ),
));?>
