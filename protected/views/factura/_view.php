<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('cliente_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cliente)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
	<?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('guia_despacho')); ?>:
	<?php echo GxHtml::encode($data->guia_despacho); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('condiciones_venta')); ?>:
	<?php echo GxHtml::encode($data->condiciones_venta); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('valor_neto')); ?>:
	<?php echo GxHtml::encode($data->valor_neto); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('iva')); ?>:
	<?php echo GxHtml::encode($data->iva); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('total')); ?>:
	<?php echo GxHtml::encode($data->total); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numero_factura')); ?>:
	<?php echo GxHtml::encode($data->numero_factura); ?>
	<br />
	*/ ?>

</div>