<?php

$this->breadcrumbs = array(
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('factura-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'factura-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
	'columns' => array(
		'numero_factura',
		array(
				'name'=>'cliente_id',
				'value'=>'GxHtml::valueEx($data->cliente)',
				'filter'=>GxHtml::listDataEx(Cliente::model()->findAllAttributes(null, true)),
				),
		'fecha',
		'guia_despacho',
		'condiciones_venta',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view}{update}{delete}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Factura',
					'url'=>'Yii::app()->createUrl("factura/ver", array("id"=>$data->id))',
				),
                                'update' => array(
					'label'=>'Actualizar Factura',
					'url'=>'Yii::app()->createUrl("factura/actualizar", array("id"=>$data->id))',
				),
                                'delete' => array(
					'label'=>'Eliminar Factura',
					'url'=>'Yii::app()->createUrl("factura/eliminar", array("id"=>$data->id))',
				),
				
			),
				
                ),
	),
)); ?>