<?php

$this->breadcrumbs = array(
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('eliminar', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Back'), 'url'=>Yii::app()->request->urlReferrer),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'factura',
			'type' => 'raw',
			'value' => $model->factura !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->factura)), array('factura/ver', 'id' => GxActiveRecord::extractPkValue($model->factura, true))) : null,
			),
'cantidad',
'servicio',
'precio_unit',
'total',

	),
)); ?>

