<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'detalle-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'factura_id'); ?>
		<?php echo $form->dropDownList($model, 'factura_id', GxHtml::listDataEx(Factura::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'factura_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->textField($model, 'cantidad'); ?>
		<?php echo $form->error($model,'cantidad'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'servicio'); ?>
		<?php echo $form->dropDownList($model, 'servicio', GxHtml::listDataEx(Servicio::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'servicio'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'precio_unit'); ?>
		<?php echo $form->textField($model, 'precio_unit'); ?>
		<?php echo $form->error($model,'precio_unit'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total'); ?>
		<?php echo $form->textField($model, 'total'); ?>
		<?php echo $form->error($model,'total'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->