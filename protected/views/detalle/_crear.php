<div class="form">

<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'detalle-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->textField($model, 'cantidad'); ?>
		<?php echo $form->error($model,'cantidad'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'servicio_id'); ?>
		<?php echo $form->dropDownList($model, 'servicio_id', GxHtml::listDataEx(Servicio::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'servicio_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'precio_unit'); ?>
		<?php echo $form->textField($model, 'precio_unit'); ?>
		<?php echo $form->error($model,'precio_unit'); ?>
		</div><!-- row -->
				

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->