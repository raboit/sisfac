<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'factura_id'); ?>
		<?php echo $form->dropDownList($model, 'factura_id', GxHtml::listDataEx(Factura::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'cantidad'); ?>
		<?php echo $form->textField($model, 'cantidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'servicio'); ?>
		<?php echo $form->dropDownList($model, 'servicio', GxHtml::listDataEx(Servicio::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'precio_unit'); ?>
		<?php echo $form->textField($model, 'precio_unit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'total'); ?>
		<?php echo $form->textField($model, 'total'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
