<?php

$this->breadcrumbs = array(
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('detalle-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'detalle-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		array(
				'name'=>'factura_id',
				'value'=>'GxHtml::valueEx($data->factura)',
				'filter'=>GxHtml::listDataEx(Factura::model()->findAllAttributes(null, true)),
				),
		'cantidad',
		'detalle',
		'precio_unit',
		'total',
		/*
		'valor_neto',
		'iva',
		'total2',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view}{update}{delete}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Detalle',
					'url'=>'Yii::app()->createUrl("detalle/ver", array("id"=>$data->id))',
				),
                                'update' => array(
					'label'=>'Actualizar Detalle',
					'url'=>'Yii::app()->createUrl("detalle/actualizar", array("id"=>$data->id))',
				),
                                'delete' => array(
					'label'=>'Eliminar Detalle',
					'url'=>'Yii::app()->createUrl("detalle/eliminar", array("id"=>$data->id))',
				),
				
			),
				
                ),
	),
)); ?>