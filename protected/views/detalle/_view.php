<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('factura_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->factura)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cantidad')); ?>:
	<?php echo GxHtml::encode($data->cantidad); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('servicio')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->servicio0)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('precio_unit')); ?>:
	<?php echo GxHtml::encode($data->precio_unit); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total')); ?>:
	<?php echo GxHtml::encode($data->total); ?>
	<br />

</div>