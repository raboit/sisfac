<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-5 last">
<?php
//    $this->widget('bootstrap.widgets.TbMenu', array(
//        'type'=>'list', // '', 'tabs', 'pills' (or 'list')
//        'stacked'=>false, // whether this is a stacked menu
//        'items'=>$this->menu,
//        'htmlOptions'=>array('class'=>'operations sidebarbootstrap well'),
//    ));
$this->widget('bootstrap.widgets.TbMenu', array(
    'type'=>'list',
    'htmlOptions'=>array('class'=>'well'),
    'items' => array_merge(
	    array(array('label'=>Yii::t('app', 'Operations'), 'itemOptions'=>array('class'=>'nav-header'))),
            $this->menu),
    ));    
?>     
</div>
<div class="span-19">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>