<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::app()->charset; ?>" />
        <meta name="language" content="<?php echo Yii::app()->language; ?>" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection, print" />
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />-->
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>
        <?php
        if (!Yii::app()->user->isGuest) {
            $user = User::model()->findByPk(Yii::app()->user->id)->tipo;
        } else {
            $user = null;
        }
        ?>

<?php echo $user ; ?>
        <div class="container" id="page">

            <?php
            switch ($user) {

                case "admin":
                    $this->widget('bootstrap.widgets.TbNavbar', array(
                        'type' => null, // null or 'inverse'
                        'brand' => Yii::app()->params['brand'],
                        'brandUrl' => array('/site/index'),
                        'collapse' => true, // requires bootstrap-responsive.css
                        'items' => array(
                            array(
                                'class' => 'bootstrap.widgets.TbMenu',
                                'items' => array(
                                    array('label' => 'Inicio', 'url' => array('/site/index')),
                                    array('label' => 'Cliente', 'url' => '#', 'items' => array(
                                            array('label' => 'Nuevo Cliente', 'url' => array('/cliente/crear')),
                                            array('label' => 'Admin Clientes', 'url' => array('/cliente/verTodos')),
                                        )),
                                    array('label' => 'Factura', 'url' => '#', 'items' => array(
                                            array('label' => 'Nueva Factura', 'url' => array('/factura/crear')),
                                            array('label' => 'Admin Factura', 'url' => array('/factura/verTodos')),
                                        )),
                                    
                                    array('label' => 'Menu', 'url' => '#', 'items' => array(
                                            array('label' => 'Usuarios', 'url' => array('/user/admin')),
                                            array('label' => 'Roles', 'url' => array('/rights/')),
                                            '---',
                                            array('label' => 'Cambiar Contraseña', 'url' => array('/user/changePassword/', "id" => Yii::app()->user->id)),
                                        )),
                                ),
                            ),
                            //'<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
                            array(
                                'class' => 'bootstrap.widgets.TbMenu',
                                'htmlOptions' => array('class' => 'pull-right'),
                                'items' => array(
                                    array('label' => 'Ingresar', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                                    array('label' => 'Salir (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                                ),
                            ),
                        ),
                    ));
                    break;
                               
                default:
                    $this->widget('bootstrap.widgets.TbNavbar', array(
                        'type' => null, // null or 'inverse'
                        'brand' => Yii::app()->params['brand'],
                        'brandUrl' => array('/site/index'),
                        'collapse' => true, // requires bootstrap-responsive.css
                        'items' => array(
                            array(
                                'class' => 'bootstrap.widgets.TbMenu',
                                'items' => array(
                                    array('label' => 'Inicio', 'url' => array('/site/index')),
                                ),
                            ),
                            //'<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
                            array(
                                'class' => 'bootstrap.widgets.TbMenu',
                                'htmlOptions' => array('class' => 'pull-right'),
                                'items' => array(
                                    array('label' => 'Ingresar', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                                    array('label' => 'Salir (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                                ),
                            ),
                        ),
                    ));
                    break; 
            }
            ?>




            <br /><br /> 
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?>
            <?php endif ?>



            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?>  Norma Ugarte y CIA. LTDA.<br/>
                Todos los derechos reservados.<br/>
                Desarrollado por <a href="http://www.raboit.com">RABO I.T.</a><br/>
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
