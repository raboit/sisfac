<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'servicio-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 200)); ?>
		<?php echo $form->error($model,'nombre'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'precio'); ?>
		<?php echo $form->textField($model, 'precio'); ?>
		<?php echo $form->error($model,'precio'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'unidad'); ?>
		<?php echo $form->textField($model, 'unidad', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'unidad'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('detalles')); ?></label>
		<?php echo $form->checkBoxList($model, 'detalles', GxHtml::encodeEx(GxHtml::listDataEx(Detalle::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->