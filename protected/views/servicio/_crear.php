<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'servicio-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 200)); ?>
		<?php echo $form->error($model,'nombre'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'precio'); ?>
		<?php echo $form->textField($model, 'precio'); ?>
		<?php echo $form->error($model,'precio'); ?>
		</div><!-- row -->
<!--		<div class="row">
                <?php echo $form->labelEx($model, 'unidad'); ?>
                <?php echo $form->dropDownList($model, 'unidad', array('c/u' => 'c/u', 'ml' => 'ml')); ?>
                <?php echo $form->error($model, 'mensaje'); ?>
                </div> row -->

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->