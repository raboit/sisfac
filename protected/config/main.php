<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// 
//Imagenes
Yii::setPathOfAlias('images',dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'images');
//X-Editable
Yii::setPathOfAlias('editable', dirname(__FILE__).'/../extensions/x-editable');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Sistema de Facturación',
        'language'=>'es',
        'sourceLanguage'=>'en',
        'charset'=>'utf-8',
        'timeZone' => 'America/Santiago',

	// preloading 'log' component
	'preload'=>array(
                'log',
                'bootstrap'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'ext.giix-components.*',
                'application.modules.rights.*',                     //rights
                'application.modules.rights.components.*',          //rights
                'editable.*'
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
                'admin'=>array(
                        'baseUrl' => '/admin',
                    ),
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'secreto',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
                        'generatorPaths' => array(
                                'ext.giix-core',
                                'bootstrap.gii'
                        ),
		),
            
                'rights'=>array(
                        'superuserName'=>'admin',
                        'authenticatedName'=>'Authenticated',
                        'userIdColumn'=>'id',
                        'userNameColumn'=>'username',
                        'enableBizRule'=>true,
                        'enableBizRuleData'=>false,
                        'displayDescription'=>true,
                        'flashSuccessKey'=>'RightsSuccess',
                        'flashErrorKey'=>'RightsError',
                        'baseUrl'=>'/rights',
                        'install'=>false,
                        'debug'=>false,
                ),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			'allowAutoLogin'=>true,
                        'class'=>'RWebUser',
		),
                
                'session' => array (
                    'class'=>'CHttpSession',                 
                    'timeout' => 36000,
                    ),
            
                'authManager'=>array(
                        'class'=>'RDbAuthManager',
                        'defaultRoles'=>array('Guest', 'Authenticated'),
                ),
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/ver',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=sisfac',
			'emulatePrepare' => true,
			'username' => 'sisfac',
			'password' => 'secreto',
			'charset' => 'utf8',
  
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
                        
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

                'bootstrap' => array(
                    'class' => 'ext.bootstrap.components.Bootstrap',
                    'responsiveCss' => true,
                ),
                'format'=>array(
                    'class'=>'application.components.Formatter',
                ),
                'editable' => array(
                    'class'     => 'editable.EditableConfig',
                    'form'      => 'bootstrap',        //form style: 'bootstrap', 'jqueryui', 'plain' 
                    'mode'      => 'popup',            //mode: 'popup' or 'inline'  
                    'defaults'  => array(              //default settings for all editable elements
                       'emptytext' => 'Click para editar'
                    )
                ),
	),
	'params'=>array(
            'brand'=>'SISFAC',
	),
);