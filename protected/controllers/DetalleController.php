<?php

class DetalleController extends GxController {


        public function calcularIva($id, $total){
            $model_factura = $this->loadModel($id, 'Factura');
            $model_factura->total = $model_factura->total + $total;  
            $model_factura->valor_neto = round($model_factura->total / 1.19);
            $model_factura->iva = round($model_factura->valor_neto * 0.19);
            $model_factura->valor_neto = $model_factura->total - $model_factura->iva;
            $model_factura->save(false);
        }
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Detalle'),
		));
	}
        
        public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Detalle'),
		));
	}

	public function actionCreate() {
		$model = new Detalle;

		$this->performAjaxValidation($model, 'detalle-form');

		if (isset($_POST['Detalle'])) {
			$model->setAttributes($_POST['Detalle']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        
        public function actionCrear($id) {
		$model = new Detalle;
                $model->factura_id = $id;
                                      
		$this->performAjaxValidation($model, 'detalle-form');

		if (isset($_POST['Detalle'])) {
			$model->setAttributes($_POST['Detalle']);
                        $model->total = $model->cantidad * $model->precio_unit;         
                        $this->calcularIva($id, $model->total);
                                
			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('factura/ver', 'id' => $id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Detalle');
                
		$this->performAjaxValidation($model, 'detalle-form');

		if (isset($_POST['Detalle'])) {
			$model->setAttributes($_POST['Detalle']);
                        $model->total = $model->cantidad * $model->precio_unit;         
                        $this->calcularIva($id, $model->total);
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Detalle');
                $total_antiguo = $model->total;
		$this->performAjaxValidation($model, 'detalle-form');

		if (isset($_POST['Detalle'])) {
			$model->setAttributes($_POST['Detalle']);
                                             
                        $model->total = $model->cantidad * $model->precio_unit;
                        $model_factura = $this->loadModel($model->factura_id, 'Factura');
                        $model_factura->total = $model_factura->total - $total_antiguo;
                        $model_factura->save(false);
                        $this->calcularIva($model->factura_id, $model->total);
                        
			if ($model->save(false)) {
				$this->redirect(array('factura/ver', 'id' => $model->factura_id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Detalle')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
        
        public function actionEliminar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$model = $this->loadModel($id, 'Detalle');
                        $id = $model->factura_id;
                        $model_factura = $this->loadModel($model->factura_id, 'Factura');
                        $model_factura->total = $model_factura->total - $model->total;
                        //$model->factura->total = $model->factura->total - $model->total;
                                                                       
                        $model_factura->save(false);
                        $this->calcularIva($model->factura_id, 0);
                        $model->delete();
                        $this->redirect(array('factura/ver','id' => $id));

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('factura/ver','id' => $id));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Detalle');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Detalle('search');
		$model->unsetAttributes();

		if (isset($_GET['Detalle']))
			$model->setAttributes($_GET['Detalle']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
		$model = new Detalle('search');
		$model->unsetAttributes();

		if (isset($_GET['Detalle']))
			$model->setAttributes($_GET['Detalle']);

		$this->render('verTodos', array(
			'model' => $model,
		));
	}

}