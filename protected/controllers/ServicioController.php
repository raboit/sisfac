<?php

class ServicioController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Servicio'),
		));
	}
        
        public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Servicio'),
		));
	}

	public function actionCreate() {
		$model = new Servicio;

		$this->performAjaxValidation($model, 'servicio-form');

		if (isset($_POST['Servicio'])) {
			$model->setAttributes($_POST['Servicio']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new Servicio;

		$this->performAjaxValidation($model, 'servicio-form');

		if (isset($_POST['Servicio'])) {
			$model->setAttributes($_POST['Servicio']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Servicio');

		$this->performAjaxValidation($model, 'servicio-form');

		if (isset($_POST['Servicio'])) {
			$model->setAttributes($_POST['Servicio']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Servicio');

		$this->performAjaxValidation($model, 'servicio-form');

		if (isset($_POST['Servicio'])) {
			$model->setAttributes($_POST['Servicio']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Servicio')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
        
        public function actionEliminar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Servicio')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('verTodos'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Servicio');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Servicio('search');
		$model->unsetAttributes();

		if (isset($_GET['Servicio']))
			$model->setAttributes($_GET['Servicio']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
		$model = new Servicio('search');
		$model->unsetAttributes();

		if (isset($_GET['Servicio']))
			$model->setAttributes($_GET['Servicio']);

		$this->render('verTodos', array(
			'model' => $model,
		));
	}

}