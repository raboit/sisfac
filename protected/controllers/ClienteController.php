<?php

class ClienteController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Cliente'),
		));
	}

        public function actionVer($id) {
            $model = $this->loadModel($id, 'Cliente');
            $model_factura = new Factura;
            $model_factura=$model_factura->misFacturas($id);
            
		$this->render('ver', array(
			'model' => $model,
                        'model_factura' => $model_factura,
		));
	}
        
	public function actionCreate() {
		$model = new Cliente;
                $session=Yii::app()->getSession();
                $id_user=$session['_id'];
                $model->user_id=$id_user;
		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new Cliente;
                $model->user_id=User::model()->findByPk(Yii::app()->user->id)->id; 
		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('verTodos', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Cliente');

		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Cliente');

		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				$this->redirect(array('verTodos', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}
        
	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Cliente')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
        
        public function actionEliminar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Cliente')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('verTodos'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Cliente');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Cliente('search');
		$model->unsetAttributes();

		if (isset($_GET['Cliente']))
			$model->setAttributes($_GET['Cliente']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
		$model = new Cliente('search');
		$model->unsetAttributes();

		if (isset($_GET['Cliente']))
			$model->setAttributes($_GET['Cliente']);

		$this->render('verTodos', array(
			'model' => $model,
		));
	}

}