<?php

class FacturaController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Factura'),
		));
	}
        public function actionImprimir($id){
            $this->layout="imprimir";
            $this->render('imprimir', array(
			'model' => $this->loadModel($id, 'Factura'),
		));
        }
        
        public function actionVer($id) {
		$model = $this->loadModel($id, 'Factura');
                $model_detalle = new Detalle;
                $model_detalle = $model_detalle->misDetalles($id);
                                    
		$this->render('ver', array(
			'model' => $model,
                        'model_detalle' => $model_detalle,
		));
                
	}

	public function actionCreate() {
		$model = new Factura;

		$this->performAjaxValidation($model, 'factura-form');

		if (isset($_POST['Factura'])) {
			$model->setAttributes($_POST['Factura']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new Factura;

		$this->performAjaxValidation($model, 'factura-form');

		if (isset($_POST['Factura'])) {
			$model->setAttributes($_POST['Factura']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Factura');

		$this->performAjaxValidation($model, 'factura-form');

		if (isset($_POST['Factura'])) {
			$model->setAttributes($_POST['Factura']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Factura');

		$this->performAjaxValidation($model, 'factura-form');

		if (isset($_POST['Factura'])) {
			$model->setAttributes($_POST['Factura']);

			if ($model->save()) {
				$this->redirect(array('verTodos', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Factura')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
        
        public function actionEliminar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Factura')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('verTodos'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Factura');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Factura('search');
		$model->unsetAttributes();

		if (isset($_GET['Factura']))
			$model->setAttributes($_GET['Factura']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
		$model = new Factura('search');
		$model->unsetAttributes();

		if (isset($_GET['Factura']))
			$model->setAttributes($_GET['Factura']);

		$this->render('verTodos', array(
			'model' => $model,
		));
	}

}