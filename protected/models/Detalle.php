<?php

Yii::import('application.models._base.BaseDetalle');

class Detalle extends BaseDetalle
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function misDetalles($id) {
		$criteria = new CDbCriteria;
		$criteria->compare('factura_id', $id);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>false,
		));
	}
}