<?php

Yii::import('application.models._base.BaseFactura');

class Factura extends BaseFactura
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public static function representingColumn() {
		return 'numero_factura';
	}
        
        public function misFacturas($id) {
		$criteria = new CDbCriteria;
		$criteria->compare('cliente_id', $id);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>false,
		));
	}
       
}
